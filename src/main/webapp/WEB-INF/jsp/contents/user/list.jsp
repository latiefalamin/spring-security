<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            User List
            <a href="<c:url value='/user/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>

    <table class="table table-striped">

        <thead>
        <td>Id</td>
        <td>Username</td>
        <td>Enabled</td>
        <td>Roles</td>
        <td>Action</td>
        </thead>

        <tbody>

        <c:forEach items="${userList}" var="user">
            <tr>
                <td><a href="<c:url value='/user?id=${user.id}'/>">${user.id}</a></td>
                <td>${user.username}</td>
                <td>${user.enabled}</td>
                <td>
                    <c:forEach items="${user.authorities}" var="role">
                        ${role.authority}&nbsp;
                    </c:forEach>
                </td>
                <td>
                    <a href="<c:url value='/user/form?id=${user.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/user/delete?id=${user.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>

        </tbody>

    </table>

</div>
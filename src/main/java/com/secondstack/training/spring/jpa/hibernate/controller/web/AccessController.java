package com.secondstack.training.spring.jpa.hibernate.controller.web;

import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccessController {

    protected static Logger logger = Logger.getLogger("controller");

    @RequestMapping("/loginpage")
	public String login(ModelMap modelMap) {
        logger.debug("Someone get a login page");
		return "login-page";
	}

    @RequestMapping("/denied")
	public String denied(ModelMap modelMap) {
        logger.debug("Someone get a denied page");
		return "denied-page";
	}

}
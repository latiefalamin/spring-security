package com.secondstack.training.spring.jpa.hibernate.domain;

import com.secondstack.training.spring.jpa.hibernate.domain.enumeration.AcademicDegree;
import com.secondstack.training.spring.jpa.hibernate.domain.enumeration.Sex;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/25/13
 * Time: 9:55 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "teacher")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "teacher_id")
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "academic_degree")
    private AcademicDegree academicDegree;

    @Column(name = "address")
    private String address;

    @Column(name = "sex")
    private Sex sex;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AcademicDegree getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(AcademicDegree academicDegree) {
        this.academicDegree = academicDegree;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}

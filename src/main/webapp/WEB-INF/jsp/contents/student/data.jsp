<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>This Student Data</h4>
    </div>
    <table>
        <tr>
            <td>First Name :</td>
            <td>${student.firstName}</td>
        </tr>
        <tr>
            <td>Last Name :</td>
            <td>${student.lastName}</td>
        </tr>
        <tr>
            <td>Address :</td>
            <td>${student.address}</td>
        </tr>
        <tr>
            <td>Sex :</td>
            <td>${student.sex}</td>
        </tr>
        <tr>
            <td>Age :</td>
            <td>${student.age}</td>
        </tr>
    </table>
    <br>
    <a href="<c:url value='/student'/>"><button class="btn">Back</button></a>
</div>

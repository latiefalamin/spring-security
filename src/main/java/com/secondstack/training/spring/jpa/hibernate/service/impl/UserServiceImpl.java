package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.security.Role;
import com.secondstack.training.spring.jpa.hibernate.domain.security.User;
import com.secondstack.training.spring.jpa.hibernate.dto.SimpleUser;
import com.secondstack.training.spring.jpa.hibernate.service.UserService;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(User user) {
        entityManager.persist(user);
    }

    @Override
    public User save(SimpleUser simpleUser) {
        User user = simpleUser.cloneTo();
        entityManager.persist(user);
        return user;
    }

    @Override
    public User update(Integer id, SimpleUser simpleUser){
        User user = simpleUser.cloneTo();
        return update(id, user);
    }

    @Override
    public User update(Integer id, User user){
        User oldUser = findById(id);
        oldUser.setAccountNonExpired(user.isAccountNonExpired());
        oldUser.setEnabled(user.isEnabled());
        oldUser.setAccountNonLocked(user.isAccountNonLocked());
        oldUser.setCredentialsNonExpired(user.isCredentialsNonExpired());
        oldUser.setAuthorities(user.getAuthorities());
        oldUser.setUsername(user.getUsername());
        oldUser.setPassword(user.getPassword());
        oldUser.setSalt(user.getSalt());
        return entityManager.merge(oldUser);
    }

    @Override
    public void delete(User user){
        entityManager.remove(user);
    }

    @Override
    public void delete(Integer id){
        entityManager.remove(findById(id));
    }

    @Override
    public List<User> findAll(){
        List<User> results = entityManager.createQuery("SELECT s from User s", User.class).getResultList();
        initialize(results);
        return results;
    }

    @Override
    public List<User> findByRole(Role role){
        List<User> results = entityManager
                .createQuery("SELECT u from User u join u.authorities a where a.authority = :role", User.class)
                .setParameter("role", role)
                .getResultList();
        initialize(results);
        return results;
    }

    @Override
    public User findById(Integer id){
        User user = entityManager.find(User.class, id);
        initialize(user);
        return user;
    }

    @Override
    public User findByUsername(String username) {
        User user = entityManager.createQuery("SELECT u from User u where u.username = :username", User.class)
                .setParameter("username", username)
                .getSingleResult();
        initialize(user);
        return user;
    }

    protected void initialize(User user){
        Hibernate.initialize(user.getAuthorities());
    }
    protected void initialize(Collection<User> users){
        for(User user:users){
            initialize(user);
        }
    }
}

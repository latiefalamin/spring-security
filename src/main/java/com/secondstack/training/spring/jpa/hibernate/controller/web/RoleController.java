package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.security.Role;
import com.secondstack.training.spring.jpa.hibernate.service.RoleService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("role")
public class RoleController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form role");

        Role role = new Role();
        modelMap.addAttribute("role", role);
        modelMap.addAttribute("roleUrl", "/role");
        modelMap.addAttribute("menuRoleClass", "active");

        return "role-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("role") Role role, ModelMap modelMap){
        logger.debug("Received request to create role");

        roleService.save(role);
        modelMap.addAttribute("role", role);
        modelMap.addAttribute("menuRoleClass", "active");

        return "role-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap){
        logger.debug("Received request to get list role");

        List<Role> roleList = roleService.findAll();
        modelMap.addAttribute("roleList", roleList);
        modelMap.addAttribute("menuRoleClass", "active");

        return "role-list-tiles";
    }

    @RequestMapping(params = {"authority"}, method = RequestMethod.GET)
    public String findById(@RequestParam("authority")String authority, ModelMap modelMap){
        logger.debug("Received request to get data role");

        Role role = roleService.findByAuthority(authority);
        modelMap.addAttribute("role", role);
        modelMap.addAttribute("menuRoleClass", "active");

        return "role-data-tiles";
    }
    @RequestMapping(value = "/delete", params = {"authority"}, method = RequestMethod.GET)
    public String delete(@RequestParam("authority")String authority, ModelMap modelMap){
        logger.debug("Received request to delete role");

        roleService.delete(authority);
        return "redirect:/role";
    }

}

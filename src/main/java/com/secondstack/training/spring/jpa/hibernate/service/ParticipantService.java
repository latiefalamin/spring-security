package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.Participant;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ParticipantService {
    void save(Participant participant);
    Participant update(Integer id, Participant participant);
    void delete(Participant participant);
    void delete(Integer id);
    List<Participant> findAll();
    Participant findById(Integer id);
}

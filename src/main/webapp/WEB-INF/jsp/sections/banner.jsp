<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row-fluid">
    <div class="span10">
        <h3 class="muted">2ndStack Spring Framework Training</h3>
    </div>
    <div class="span2">
        <h5 class="muted" align="right">
            Hi <sec:authentication property="principal.username"/>!
            <br>
            <a href="<c:url value='/logout'/>">Sign out</a>
        </h5>
    </div>
</div>